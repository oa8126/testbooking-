#                                  This code is for testing our booking logic 

# intro
First we will work on 24 model to simplify the problem so we are only concerned about one day 
In other words imagine the application only works for one day then earth will be destroyed so no need to think about the future 
Second we will get rid of the variables that don’t concern the core of the algorithm like available areas, customer address, service name ...etc 

# bookLib.py
This lib is for the funtions only 

# bookClass.py
This is for the classes only 

# booking.py
This is the main program 