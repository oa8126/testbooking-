from bookClass import *
def newCustomerGreeting(customerID,admin):
    """creat new customer and display service menue"""
    serviceList = list()
    for k in range(0, 15):
        print("\n")
    print("hello Customer ID=",customerID,"we have ")
    for i in range(0,admin.nS):
        serviceList.append(admin.S[i].name+" ")
    print(serviceList)

def getAdminServiceList(nS):
    """get service list form admin"""
    S=list()
    for i in range(0,nS):
        print(" ")
        while(True):
            adminChoise=input("duration of Service "+str(i+1)+" in hours ")
            if(is_number(adminChoise)):
                break
            else:
                print("\nnot a numbe\n")
        S.append(Service(adminChoise,"s"+str(i+1))) #"s"+str(i+1) to auto name servises
    return S

def getAdminTruckist(nT):
    """get Truck list form admin"""
    T=list()
    for i in range(0,nT):
        T.append(Truck(i+1))
    return T


def getCustomerServise(admin):
    """getCustomer service choise and insures it is on the list"""
    sTemp=True
    while(sTemp):
        customerS = input(" wrtie the s you want ")
        for i in range(0,admin.nS):
            if(admin.S[i].name==customerS):
                newCustomerServiceChoice = admin.S[i]
                sTemp=False
        if(sTemp==True):
            print(" not in our s list ")
    return newCustomerServiceChoice

def getCustomerTime(admin):
    """getCustomer choise of time and insures it is correct format"""
    timeTemp = True
    while(timeTemp):
        timeTemp=False
        try:
            print(" ")
            customerT = float(input("choose time between "+str(admin.ot1)+" and "+ str(admin.ot2)+" either int or .5 for half hour ex: 10 or 10.5 "))
            if(customerT < admin.ot1 or customerT>admin.ot2):
                print("sorry we are close this time choose another time ")
                timeTemp=True
            if((customerT-int(customerT) != 0)):
                if((customerT-int(customerT) != 0.5)):
                    print(customerT," wrong choise did you mean ", int(customerT)+0.5," ? try again ")
                    timeTemp=True

        except ValueError:
            print("not a number choose time again")
            timeTemp=True
    return customerT

def getCustomerDuration(newCustomer,admin):
    """get hou much the new customer will take to servie in hours"""
    time = float(newCustomer.s.duration)+ (admin.rT/60)
    return time

def doesIntersect(a,b,c,d):
    """if new customer time intersects with old one the function returns True"""
    ans=False
    a = int(a*60) #oldCustomerStartTime converted to minutes
    b = int(b*60) #oldCustomerEndTime converted to minutes
    c = int(c*60) #newCustomerStartTime converted to minutes
    d = int(d*60) #newCustomerEndTime converted to minutes
    x = range(a,b)
    y = range(c,d)
    xs = set(x)
    if(len(xs.intersection(y))!=0):
        ans=True
    return ans

def cheakIfTruckAvailability(T,newCustomer):
    """cheaks if Truk can take the order"""
    stat=True
    for j in range(0,len(T.customers)):
        oldCustomerStartTime = float(T.customers[j].ct)
        oldCustomerEndTime  =  oldCustomerStartTime + float(T.customers[j].s.duration)
        currentCustomerStartTime = float(newCustomer.ct)
        currentCustomerEndime = currentCustomerStartTime + float(newCustomer.s.duration)
        if(doesIntersect(oldCustomerStartTime,oldCustomerEndTime,currentCustomerStartTime,currentCustomerEndime)):
            stat=False
            break
    return stat

def isOptimal(admin,T):
    """if truk has least number of customers function returns true Note:when we hook to google maps we count for location for now only number of customers"""
    ans=True
    numberOfCustomersInThisTruck=len(T.customers)
    for i in range(0, admin.nT):
        if(numberOfCustomersInThisTruck>len(admin.T[i].customers)):
            ans=False
    return ans

def programMenue(admin):
    """display menue"""
    stillOpen = True
    userInput = input("listening for a new customer, enter e to export a report  q to quit or any key for a new customer or p to print report ")
    if(userInput == "q"):
        stillOpen=False
    elif(userInput=="e"):
        exportReport(admin,"report-example.txt")
        userInput = input(" enter q to quit or any key for a new customer ")
        if(userInput == "q"):
            stillOpen=False
    elif(userInput=="p"):
        for i in range(0, 15):
            print("\n")
        for i in range(0,admin.nT):
            if(admin.T[i].stat == True):
                for j in range(0, len(admin.T[i].customers)):
                    if(admin.T[i].customers[j].id==0):
                        print("Truck id=",admin.T[i].id, " (first order) will go to customer id=",admin.T[i].customers[j].id, "his choise of service is ",admin.T[i].customers[j].s.name," truck will go to him at ", admin.T[i].customers[j].ct, " and will finish at ", float(admin.T[i].customers[j].ct)+float(admin.T[i].customers[j].s.duration))
                    else:
                        print("Truck id=",admin.T[i].id, " will go to customer id=",admin.T[i].customers[j].id, "his choise of service is ",admin.T[i].customers[j].s.name," truck will go to him at ", admin.T[i].customers[j].ct, " and will finish at ", float(admin.T[i].customers[j].ct)+float(admin.T[i].customers[j].s.duration))
        for k in range(0, 5):
            print("\n")
        userInput = input(" enter q to quit or any key for a new customer ")
        if(userInput == "q"):
            stillOpen=False
    return stillOpen

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    return False

def exportReport(admin, filename):
    #This function takes the name of a file and opens that file.\
    with open(filename,'w') as file:
        file.write("Booking-time    "+'Truck-id'+'     '+' Customer-id'+'     '+ 'Service-Name'+'\n'+"----------------------------------------------------------")
    for i in range(0, 15):
        print("\n")
    for i in range(0,admin.nT):
        if(admin.T[i].stat == True):
            for j in range(0, len(admin.T[i].customers)):
                with open(filename,'a') as file:
                        startTime=str(admin.T[i].customers[j].ct)
                        endTime=str(float(admin.T[i].customers[j].ct)+float(admin.T[i].customers[j].s.duration))
                        TruckId=str(admin.T[i].id)
                        CustomerIid=str(admin.T[i].customers[j].id)
                        ServiceName=str(admin.T[i].customers[j].s.name)
                        if(float(startTime)<10 and float(endTime)<10):
                            file.write("\n"+startTime+"--"+endTime+"     |       "+TruckId+'     |       '+CustomerIid+'      |      '+ ServiceName+"\n")
                        elif(float(startTime)>9 and float(endTime)>9):
                            file.write("\n"+startTime+"--"+endTime+"   |       "+TruckId+'     |       '+CustomerIid+'      |      '+ ServiceName+"\n")
                        else:
                            file.write("\n"+startTime+"--"+endTime+"    |       "+TruckId+'     |       '+CustomerIid+'      |      '+ ServiceName+"\n")
