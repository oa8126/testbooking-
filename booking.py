from bookLib import *
nT=3 #Number of available trucks in inventory Ex: nT = 10
ot1=6 #open time
ot2=23 #close time
nS=3 #number of Services
rT=0 #Random time which the admin has to estimate for how long in minutes it might take for a Truck to go from location A to B it is not perfect so the admin can give a number the goes a little bit above average to be safe. Ex: RT = 25. In later stage we can eliminate admin input and replace it with google api that tells the estimated time to go from A to B based on the previous reservation of the truck.
admin = Admin(nT,ot1,ot2,nS,rT)
customerIDgiver=1
stillOpen=True
while(stillOpen):
    newCustomerGreeting(customerIDgiver,admin) #great new customer and display service
    #service collection logic
    newCustomerServiceChoice = getCustomerServise(admin)#getCustomer service choise and insures it is on the list
    #cheak and set cheakIfTruckAvailability for each truck in the inventory then asigne optimal
    didNotBook=True
    while(didNotBook):
        customerT= getCustomerTime(admin) #getCustomer choise of time and insures it is correct format
        newCustomer = Customer(newCustomerServiceChoice,customerT,customerIDgiver) #make an instant of customer object
        newCustomer.s.duration = getCustomerDuration(newCustomer,admin) #get hou much the new customer will take to servie in hours
        for i in range(0,admin.nT):  #loop trhough all Truck
            if(cheakIfTruckAvailability(admin.T[i],newCustomer)): #if this Truck can take the new order
                admin.T[i].stat = True # set this truck stat to True
                if(isOptimal(admin,admin.T[i])): #if this truk is the optimal truck to choose
                    admin.T[i].customers.append(newCustomer) # add the new order to the Truck
                    customerIDgiver+=1 #update id
                    didNotBook=False #indicate that the booking is sucseesful
                    print("\napproved correct order\n")
                    break
        if(didNotBook):
            print(" ")
            print("sorry all our trucks are booked in a time that intersects with your choise of time please choose another time please")
    stillOpen = programMenue(admin) #end of program menue
